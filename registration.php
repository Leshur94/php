<?php 
	session_start();
	require 'database.php';

	if (isset($_POST['login']))
	{
		$continue=true; 
		$login=$_POST['login'];
		if ((strlen($login)<5) or (strlen($login)>10)) {
			$_SESSION['login_error']='<span style="color:red"> Poprawny login powinien mieć od 5 do 10 znaków </span>';	
			$continue=false;
		}	
		if(ctype_alnum($login)==false)	{
			$_SESSION['login_error']='<span style="color:red"> Poprawny login powinien składać się wyłącznie z liter i cyfr(bez polskich znaków)! </span>';
			$continue=false;
		}
		$password=$_POST['password'];
		$password2=$_POST['password2'];
		if((strlen($password)<6) or (strlen($password)>30))
		{
		$_SESSION['password_error']='<span style="color:red"> Poprawne hasło powinino mieć od 6 do 30 znaków </span>';	
		$continue=false;
		}	
			if($password!=$password2)
			{
			$_SESSION['password_error']='<span style="color:red"> Podane hasła różnią się! </span>';	
			$continue=false;
			}	
				$password_hashed=password_hash($password,PASSWORD_DEFAULT);
		
		$email=$_POST['email'];
		
		$emailvalid= filter_var($email, FILTER_SANITIZE_EMAIL);
		if((filter_var($email, FILTER_VALIDATE_EMAIL)==false) or ($emailvalid!=$email))
		{
		$_SESSION['email_error']='<span style="color:red"> Błędny adres email! </span>';
		$continue=false;
		}
			if(!isset($_POST['rules']))
			{
			$_SESSION['rules_error']='<span style="color:red"> Zaakceptuj regulamin! </span>';
			$continue=false;
			}
		
		try
		{
		$connection= new mysqli($host, $db_user, $db_password, $db_name); 
			if($connection->connect_errno!=0)
			{
			throw new Exception(mysqli_connect_errno());
			}
			
				else
				{ $result = $connection->query("SELECT id FROM users WHERE email='$email'");
			
					if (!$result) 
					{
					throw new Exception($connection->error); 
					}
						$n_o_m=$result->num_rows;
							if($n_o_m>0)
							{
							$_SESSION['email_error']='<span style="color:red"> Adres email jest już używany! </span>';
							$continue=false;
							}

			$result = $connection->query("SELECT id FROM users WHERE login='$login'");
			if(!$result) 
			{
			throw new Exception($connection->error); 
			}
				$n_o_l=$result->num_rows;
					if($n_o_l>0)
					{
					$_SESSION['login_error']='<span style="color:red"> Login jest już używany! </span>';
					$continue=false;
					}
						if ($continue==true){
							if($connection->query("INSERT INTO users VALUES (NULL, '$login','$password_hashed','$email', '100')"))
								{
									$_SESSION['succes']=true;
									header('Location: succes.php');
								}
								else
									{
										throw new Exception($connection->error);
									}
			}
		
			 $connection->close();
			 }
		}
		catch(Exception $exc)
		{
			
		echo '<span style="color:red"> Błąd bazy danych. </span>';
		}
		
	
	}
	

?>






<!DOCETYPE HTML>
<html lang="pl">
<head>
	<meta charset="utf-8" />
	<title>Rejestracja	</title>
	<meta http-equiv="X-Ua-Compatible" content="IE=edge,chrome=1" />
</head>


<body>
	<form method="post">
		Login:	<br />	<input type="text" name="login">	<br />
			<?php
			if(	isset($_SESSION['login_error']))
			{
				echo $_SESSION['login_error']."<br/>"; 
				unset($_SESSION['login_error']);
			}
			?>
		Hasło:  <br />	<input type="password" name="password">	<br />
			<?php
			if(	isset($_SESSION['password_error']))
			{
				echo $_SESSION['password_error']."<br/>";
				unset($_SESSION['password_error']);
			}
			?>
		Powtórz hasło:  <br />	<input type="password" name="password2">	<br />
		
		Podaj adres email:  <br />	<input type="text" name="email">	<br />
			<?php
			if(	isset($_SESSION['email_error']))
			{
				echo ($_SESSION['email_error']."<br/>");
				unset($_SESSION['email_error']);
			}
			?>
		Zapoznałem sie i akceptuje regulamin: 	<input type="checkbox" name="rules"> <a href="rules.html" target="blank"> Regulamin</a>	<br />
		
			<?php
			if(	isset($_SESSION['rules_error']))
			{
				echo ($_SESSION['rules_error']."<br/>");
				unset($_SESSION['rules_error']);
			}
			?>
			
		<input type="submit" value="Wyślij">	
	</form>
</body>
</html>
